﻿using System.Linq;
using System.Web;

namespace Piotr.XamlSignalR.Service.Data
{
    public class Quote
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal PriceChange { get; set; }
    }
}